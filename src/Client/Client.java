package Client;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Socket сlientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;
    private static String inMessage;
    private static String outMessage;
    private static String isExit;
    //  Thread wait and get massages from server
    private static Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    in = new BufferedReader(new InputStreamReader(сlientSocket.getInputStream()));
                    outMessage = in.readLine();
                    System.out.println("User...: " + outMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    });

    public static void main(String[] args) {
        // Throw messages to server
        try {
            сlientSocket = new Socket("127.0.0.1", 22233);
            thread.start();
            if(сlientSocket.isConnected() == true) {
                while (true) {
                    reader = new BufferedReader(new InputStreamReader(System.in));
                    inMessage = reader.readLine();
                    isExit = inMessage;
                    out = new BufferedWriter(new OutputStreamWriter(сlientSocket.getOutputStream()));
                    out.write(inMessage + "\n");
                    out.flush();
                }
            } else {
                System.out.println("Ошибка соединения");
                try {
                    reader.close();
                    in.close();
                    out.close();
                    сlientSocket.close();
                } catch (IOException ex) {
                    System.out.println("ERROR");
                }
            }
        } catch (Exception e) {
            System.out.println("Сервер офлайн...");
        }
        System.out.println("ERROR");
    }
}
